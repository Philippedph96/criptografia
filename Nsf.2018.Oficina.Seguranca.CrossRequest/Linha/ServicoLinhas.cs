﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Oficina.Seguranca.CrossRequest.Linha
{
    class ServicoLinhas
    {
        public List<LinhaDTO> Buscar(string linha)
        {
            HttpClient rest = new HttpClient();

            var session = rest.PostAsync("http://olhovivo.sptrans.com.br/", null).Result.Content.ReadAsStringAsync().Result;
            var content = rest.GetAsync($"http://olhovivo.sptrans.com.br/data/Linha/Buscar?termosBusca={linha}").Result.Content.ReadAsStringAsync().Result;

            List<LinhaDTO> linhas = JsonConvert.DeserializeObject<List<LinhaDTO>>(content);
            return linhas;
        }
    }
}

