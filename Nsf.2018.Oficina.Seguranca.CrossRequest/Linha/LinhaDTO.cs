﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Oficina.Seguranca.CrossRequest.Linha
{
    class LinhaDTO
    {
        [JsonProperty(PropertyName = "lt")]
        public string Prefixo { get; set; }


        [JsonProperty(PropertyName = "tp")]
        public string Origem { get; set; }


        [JsonProperty(PropertyName = "ts")]
        public string Destino { get; set; }
    }

}
