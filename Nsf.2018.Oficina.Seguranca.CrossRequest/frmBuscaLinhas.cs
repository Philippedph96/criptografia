﻿using Nsf._2018.Oficina.Seguranca.CrossRequest.Linha;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Oficina.Seguranca.CrossRequest
{
    public partial class frmBuscaLinhas : Form
    {
        public frmBuscaLinhas()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            ServicoLinhas servicoLinhas = new ServicoLinhas();
            List<LinhaDTO> linhas = servicoLinhas.Buscar(txtLinha.Text);

            dgvLinhas.AutoGenerateColumns = false;
            dgvLinhas.DataSource = linhas;
        }


    }


}
