﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Oficina.Seguranca.Phishing.Email
{
    class PhishingEmail
    {
        public void Enviar(string para)
        {
            Task.Factory.StartNew(() =>
            {
                // Se for usar seu email, habilite ele em: https://myaccount.google.com/lesssecureapps?pli=1
                string remetente = "nsfmailing@gmail.com";
                string senha = "nsf@mailing";


                string assunto = "Facebook | Troque sua senha";
                string mensagem = CriarMensagemComHtml();

                // Configura a mensagem
                MailMessage email = new MailMessage();

                // Configura Remetente, Destinatário
                email.From = new MailAddress(remetente);
                email.To.Add(para);

                // Configura Assunto, Corpo e se o Corpo está em Html
                email.Subject = assunto;
                email.Body = mensagem;
                email.IsBodyHtml = true;


                // Configura os parâmetros do objeto SMTP
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;


                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(remetente, senha);

                // Envia a mensagem
                smtp.Send(email);

            });
        }

        private string CriarMensagemComHtml()
        {
            // Lê o html do arquivo email.html
            string html = System.IO.File.ReadAllText("Email/email.html");
            return html;
        }

    }
}
