﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Oficina.Seguranca.Phishing
{
    public partial class frmPhishing : Form
    {
        public frmPhishing()
        {
            InitializeComponent();
        }

        private void btnPescar_Click(object sender, EventArgs e)
        {
            Email.PhishingEmail email = new Email.PhishingEmail();
            email.Enviar(txtEmail.Text);
        }
    }
}
