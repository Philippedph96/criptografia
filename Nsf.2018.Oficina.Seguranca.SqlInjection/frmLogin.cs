﻿using Nsf._2018.Oficina.Seguranca.SqlInjection.DB.Injection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Oficina.Seguranca.SqlInjection
{
    public partial class frmLogin : Form
    {
        
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            LoginBusiness business = new LoginBusiness();
            bool logou = business.Logar(txtUsuario.Text, txtSenha.Text);

            if (logou)
            {
                frmMenu tela = new frmMenu();
                tela.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.");
            }
        }
    }
}
