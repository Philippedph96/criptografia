﻿using MySql.Data.MySqlClient;
using Nsf._2018.Oficina.Seguranca.SqlInjection.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Oficina.Seguranca.SqlInjection.DB.Injection
{
    class LoginDatabase
    {
        // Para dropar o banco que não usa segurança MySqlParameter
        // SqlInjection: '; drop database InjectionDB; --

        public bool Logar(string usuario, string senha)
        {
            string script = $@"SELECT * 
                                 FROM tb_login 
                                WHERE ds_usuario = '{usuario}'
                                  AND ds_senha   = '{senha}'";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
