﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Oficina.Seguranca.SqlInjection.DB.Injection
{
    class LoginBusiness
    {
        public bool Logar(string usuario, string senha)
        {
            LoginDatabase db = new LoginDatabase();
            return db.Logar(usuario, senha);
        }
    }
}
