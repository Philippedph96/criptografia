console.log('Iniciando Site');


var express = require("express");
var app = express();
var fs = require('fs');


app.get('/index.html', function (req, res) {
    var filePath = __dirname + "/public/imagens/kongfu2.jpg";
    fs.readFile(filePath, function (e, imageBuffer) {
        if (e) return res.json({ status: 500, value: e });

        res.writeHead(200, { 'Content-Type': 'image/jpg' });
        res.end(imageBuffer);
    });

});

app.get('/facebook.html', function (req, res) {
    var filePath = __dirname + "/public/paginas/facebook.html";
    fs.readFile(filePath, function (e, imageBuffer) {
        if (e) return res.json({ status: 500, value: e });

        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(imageBuffer);
    });

});


var port = 8090;

var server = app.listen(port, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('===========================================================');
    console.log("Listening at http://%s:%s", host, port)
});


