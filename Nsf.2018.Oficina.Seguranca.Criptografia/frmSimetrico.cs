﻿using Nsf._2018.Oficina.Seguranca.Criptografia.Criptografia.Simetrico;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Oficina.Seguranca.Criptografia
{
    public partial class frmSimetrico : Form
    {
        public frmSimetrico()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAESCript_Click(object sender, EventArgs e)
        {
            AESCript cript = new AESCript();
            string textoCript = cript.Criptografar(txtChaveAES.Text, txtAESTexto.Text);

            txtAESCript.Text = textoCript;
        }

        private void btnAESDescript_Click(object sender, EventArgs e)
        {
            AESCript cript = new AESCript();
            string texto = cript.Descriptografar(txtChaveAES.Text, txtAESCript.Text);

            txtAESTexto.Text = texto;
        }

        private void btnDESCript_Click(object sender, EventArgs e)
        {
            DESCript cript = new DESCript();
            string textoCript = cript.Criptografar(txtDESChave.Text, txtDESTexto.Text);

            txtDESCRipt.Text = textoCript;
        }

        private void btnDESDecript_Click(object sender, EventArgs e)
        {
            DESCript cript = new DESCript();
            string texto = cript.Descriptografar(txtDESChave.Text, txtDESCRipt.Text);

            txtDESTexto.Text = texto;
        }

        private void btnDES3Cript_Click(object sender, EventArgs e)
        {
            DES3Cript cript = new DES3Cript();
            string textoCript = cript.Criptografar(txtDES3Chave.Text, txtDES3Texto.Text);

            txtDES3Cript.Text = textoCript;
        }

        private void btnDES3Decript_Click(object sender, EventArgs e)
        {
            DES3Cript cript = new DES3Cript();
            string texto = cript.Descriptografar(txtDES3Chave.Text, txtDES3Cript.Text);

            txtDES3Texto.Text = texto;
        }
    }
}
