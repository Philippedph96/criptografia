﻿using Nsf._2018.Oficina.Seguranca.Criptografia.Criptografia.Hash;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Oficina.Seguranca.Criptografia
{
    public partial class frmHash : Form
    {
        public frmHash()
        {
            InitializeComponent();
        }

        

        private void btnMD5Cript_Click(object sender, EventArgs e)
        {
            MD5Cript cript = new MD5Cript();
            string textoCript = cript.Criptografar(txtMD5Texto.Text);

            txtMD5Cript.Text = textoCript;
        }

        private void btnSHACript_Click(object sender, EventArgs e)
        {
            SHA256Cript cript = new SHA256Cript();
            string textoCript = cript.Criptografar(txtSHATexto.Text);

            txtSHACript.Text = textoCript;
        }
    }
}
