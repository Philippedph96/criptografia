﻿namespace Nsf._2018.Oficina.Seguranca.Criptografia
{
    partial class frmHash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMD5Cript = new System.Windows.Forms.Button();
            this.txtMD5Cript = new System.Windows.Forms.TextBox();
            this.txtMD5Texto = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSHACript = new System.Windows.Forms.Button();
            this.txtSHACript = new System.Windows.Forms.TextBox();
            this.txtSHATexto = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(140)))), ((int)(((byte)(93)))));
            this.panel1.Controls.Add(this.btnMD5Cript);
            this.panel1.Controls.Add(this.txtMD5Cript);
            this.panel1.Controls.Add(this.txtMD5Texto);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(493, 279);
            this.panel1.TabIndex = 20;
            // 
            // btnMD5Cript
            // 
            this.btnMD5Cript.FlatAppearance.BorderSize = 2;
            this.btnMD5Cript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMD5Cript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMD5Cript.ForeColor = System.Drawing.Color.White;
            this.btnMD5Cript.Location = new System.Drawing.Point(28, 221);
            this.btnMD5Cript.Name = "btnMD5Cript";
            this.btnMD5Cript.Size = new System.Drawing.Size(216, 35);
            this.btnMD5Cript.TabIndex = 3;
            this.btnMD5Cript.Text = "MD5 - Cript";
            this.btnMD5Cript.UseVisualStyleBackColor = true;
            this.btnMD5Cript.Click += new System.EventHandler(this.btnMD5Cript_Click);
            // 
            // txtMD5Cript
            // 
            this.txtMD5Cript.BackColor = System.Drawing.Color.Black;
            this.txtMD5Cript.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMD5Cript.ForeColor = System.Drawing.Color.White;
            this.txtMD5Cript.Location = new System.Drawing.Point(250, 22);
            this.txtMD5Cript.Multiline = true;
            this.txtMD5Cript.Name = "txtMD5Cript";
            this.txtMD5Cript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMD5Cript.Size = new System.Drawing.Size(216, 193);
            this.txtMD5Cript.TabIndex = 2;
            // 
            // txtMD5Texto
            // 
            this.txtMD5Texto.Location = new System.Drawing.Point(28, 22);
            this.txtMD5Texto.Multiline = true;
            this.txtMD5Texto.Name = "txtMD5Texto";
            this.txtMD5Texto.Size = new System.Drawing.Size(216, 193);
            this.txtMD5Texto.TabIndex = 1;
            this.txtMD5Texto.Text = "Texto";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(493, 64);
            this.panel2.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Tomato;
            this.label2.Location = new System.Drawing.Point(449, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "x";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Harlow Solid Italic", 22F, System.Drawing.FontStyle.Italic);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Criptografia Hash";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(74)))));
            this.panel3.Controls.Add(this.btnSHACript);
            this.panel3.Controls.Add(this.txtSHACript);
            this.panel3.Controls.Add(this.txtSHATexto);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 343);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(493, 286);
            this.panel3.TabIndex = 21;
            // 
            // btnSHACript
            // 
            this.btnSHACript.FlatAppearance.BorderSize = 2;
            this.btnSHACript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSHACript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSHACript.ForeColor = System.Drawing.Color.White;
            this.btnSHACript.Location = new System.Drawing.Point(28, 221);
            this.btnSHACript.Name = "btnSHACript";
            this.btnSHACript.Size = new System.Drawing.Size(216, 35);
            this.btnSHACript.TabIndex = 3;
            this.btnSHACript.Text = "SHA - Cript";
            this.btnSHACript.UseVisualStyleBackColor = true;
            this.btnSHACript.Click += new System.EventHandler(this.btnSHACript_Click);
            // 
            // txtSHACript
            // 
            this.txtSHACript.BackColor = System.Drawing.Color.Black;
            this.txtSHACript.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSHACript.ForeColor = System.Drawing.Color.White;
            this.txtSHACript.Location = new System.Drawing.Point(250, 22);
            this.txtSHACript.Multiline = true;
            this.txtSHACript.Name = "txtSHACript";
            this.txtSHACript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSHACript.Size = new System.Drawing.Size(216, 193);
            this.txtSHACript.TabIndex = 2;
            // 
            // txtSHATexto
            // 
            this.txtSHATexto.Location = new System.Drawing.Point(28, 22);
            this.txtSHATexto.Multiline = true;
            this.txtSHATexto.Name = "txtSHATexto";
            this.txtSHATexto.Size = new System.Drawing.Size(216, 193);
            this.txtSHATexto.TabIndex = 1;
            this.txtSHATexto.Text = "Texto";
            // 
            // frmHash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 629);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmHash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Oficina - Criptografia";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMD5Cript;
        private System.Windows.Forms.TextBox txtMD5Cript;
        private System.Windows.Forms.TextBox txtMD5Texto;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSHACript;
        private System.Windows.Forms.TextBox txtSHACript;
        private System.Windows.Forms.TextBox txtSHATexto;
    }
}

