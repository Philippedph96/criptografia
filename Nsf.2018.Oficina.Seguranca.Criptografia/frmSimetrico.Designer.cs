﻿namespace Nsf._2018.Oficina.Seguranca.Criptografia
{
    partial class frmSimetrico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnDES3Decript = new System.Windows.Forms.Button();
            this.btnDES3Cript = new System.Windows.Forms.Button();
            this.txtDES3Cript = new System.Windows.Forms.TextBox();
            this.txtDES3Texto = new System.Windows.Forms.TextBox();
            this.txtDES3Chave = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDESDecript = new System.Windows.Forms.Button();
            this.btnDESCript = new System.Windows.Forms.Button();
            this.txtDESCRipt = new System.Windows.Forms.TextBox();
            this.txtDESTexto = new System.Windows.Forms.TextBox();
            this.txtDESChave = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAESDescript = new System.Windows.Forms.Button();
            this.btnAESCript = new System.Windows.Forms.Button();
            this.txtAESCript = new System.Windows.Forms.TextBox();
            this.txtAESTexto = new System.Windows.Forms.TextBox();
            this.txtChaveAES = new System.Windows.Forms.TextBox();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(493, 64);
            this.panel4.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Tomato;
            this.label2.Location = new System.Drawing.Point(449, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "x";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Harlow Solid Italic", 22F, System.Drawing.FontStyle.Italic);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Criptografia Simétrica";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(192)))), ((int)(((byte)(183)))));
            this.panel3.Controls.Add(this.btnDES3Decript);
            this.panel3.Controls.Add(this.btnDES3Cript);
            this.panel3.Controls.Add(this.txtDES3Cript);
            this.panel3.Controls.Add(this.txtDES3Texto);
            this.panel3.Controls.Add(this.txtDES3Chave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 596);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(493, 279);
            this.panel3.TabIndex = 24;
            // 
            // btnDES3Decript
            // 
            this.btnDES3Decript.FlatAppearance.BorderSize = 2;
            this.btnDES3Decript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDES3Decript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDES3Decript.ForeColor = System.Drawing.Color.White;
            this.btnDES3Decript.Location = new System.Drawing.Point(250, 220);
            this.btnDES3Decript.Name = "btnDES3Decript";
            this.btnDES3Decript.Size = new System.Drawing.Size(216, 35);
            this.btnDES3Decript.TabIndex = 14;
            this.btnDES3Decript.Text = "DES3 - Descript";
            this.btnDES3Decript.UseVisualStyleBackColor = true;
            this.btnDES3Decript.Click += new System.EventHandler(this.btnDES3Decript_Click);
            // 
            // btnDES3Cript
            // 
            this.btnDES3Cript.FlatAppearance.BorderSize = 2;
            this.btnDES3Cript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDES3Cript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDES3Cript.ForeColor = System.Drawing.Color.White;
            this.btnDES3Cript.Location = new System.Drawing.Point(28, 220);
            this.btnDES3Cript.Name = "btnDES3Cript";
            this.btnDES3Cript.Size = new System.Drawing.Size(216, 35);
            this.btnDES3Cript.TabIndex = 13;
            this.btnDES3Cript.Text = "DES3 - Cript";
            this.btnDES3Cript.UseVisualStyleBackColor = true;
            this.btnDES3Cript.Click += new System.EventHandler(this.btnDES3Cript_Click);
            // 
            // txtDES3Cript
            // 
            this.txtDES3Cript.BackColor = System.Drawing.Color.Black;
            this.txtDES3Cript.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDES3Cript.ForeColor = System.Drawing.Color.White;
            this.txtDES3Cript.Location = new System.Drawing.Point(250, 21);
            this.txtDES3Cript.Multiline = true;
            this.txtDES3Cript.Name = "txtDES3Cript";
            this.txtDES3Cript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDES3Cript.Size = new System.Drawing.Size(216, 193);
            this.txtDES3Cript.TabIndex = 12;
            // 
            // txtDES3Texto
            // 
            this.txtDES3Texto.Location = new System.Drawing.Point(28, 60);
            this.txtDES3Texto.Multiline = true;
            this.txtDES3Texto.Name = "txtDES3Texto";
            this.txtDES3Texto.Size = new System.Drawing.Size(216, 154);
            this.txtDES3Texto.TabIndex = 11;
            this.txtDES3Texto.Text = "Texto";
            // 
            // txtDES3Chave
            // 
            this.txtDES3Chave.Location = new System.Drawing.Point(28, 21);
            this.txtDES3Chave.Name = "txtDES3Chave";
            this.txtDES3Chave.Size = new System.Drawing.Size(216, 33);
            this.txtDES3Chave.TabIndex = 10;
            this.txtDES3Chave.Text = "chavede24digitos!!!!!!!!";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(171)))), ((int)(((byte)(158)))));
            this.panel2.Controls.Add(this.btnDESDecript);
            this.panel2.Controls.Add(this.btnDESCript);
            this.panel2.Controls.Add(this.txtDESCRipt);
            this.panel2.Controls.Add(this.txtDESTexto);
            this.panel2.Controls.Add(this.txtDESChave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 335);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(493, 261);
            this.panel2.TabIndex = 23;
            // 
            // btnDESDecript
            // 
            this.btnDESDecript.FlatAppearance.BorderSize = 2;
            this.btnDESDecript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDESDecript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDESDecript.ForeColor = System.Drawing.Color.White;
            this.btnDESDecript.Location = new System.Drawing.Point(250, 211);
            this.btnDESDecript.Name = "btnDESDecript";
            this.btnDESDecript.Size = new System.Drawing.Size(216, 35);
            this.btnDESDecript.TabIndex = 9;
            this.btnDESDecript.Text = "DES - Descript";
            this.btnDESDecript.UseVisualStyleBackColor = true;
            this.btnDESDecript.Click += new System.EventHandler(this.btnDESDecript_Click);
            // 
            // btnDESCript
            // 
            this.btnDESCript.FlatAppearance.BorderSize = 2;
            this.btnDESCript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDESCript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDESCript.ForeColor = System.Drawing.Color.White;
            this.btnDESCript.Location = new System.Drawing.Point(28, 211);
            this.btnDESCript.Name = "btnDESCript";
            this.btnDESCript.Size = new System.Drawing.Size(216, 35);
            this.btnDESCript.TabIndex = 8;
            this.btnDESCript.Text = "DES - Cript";
            this.btnDESCript.UseVisualStyleBackColor = true;
            this.btnDESCript.Click += new System.EventHandler(this.btnDESCript_Click);
            // 
            // txtDESCRipt
            // 
            this.txtDESCRipt.BackColor = System.Drawing.Color.Black;
            this.txtDESCRipt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDESCRipt.ForeColor = System.Drawing.Color.White;
            this.txtDESCRipt.Location = new System.Drawing.Point(250, 12);
            this.txtDESCRipt.Multiline = true;
            this.txtDESCRipt.Name = "txtDESCRipt";
            this.txtDESCRipt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDESCRipt.Size = new System.Drawing.Size(216, 193);
            this.txtDESCRipt.TabIndex = 7;
            // 
            // txtDESTexto
            // 
            this.txtDESTexto.Location = new System.Drawing.Point(28, 51);
            this.txtDESTexto.Multiline = true;
            this.txtDESTexto.Name = "txtDESTexto";
            this.txtDESTexto.Size = new System.Drawing.Size(216, 154);
            this.txtDESTexto.TabIndex = 6;
            this.txtDESTexto.Text = "Texto";
            // 
            // txtDESChave
            // 
            this.txtDESChave.Location = new System.Drawing.Point(28, 12);
            this.txtDESChave.Name = "txtDESChave";
            this.txtDESChave.Size = new System.Drawing.Size(216, 33);
            this.txtDESChave.TabIndex = 5;
            this.txtDESChave.Text = "chave8dg";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(151)))), ((int)(((byte)(136)))));
            this.panel1.Controls.Add(this.btnAESDescript);
            this.panel1.Controls.Add(this.btnAESCript);
            this.panel1.Controls.Add(this.txtAESCript);
            this.panel1.Controls.Add(this.txtAESTexto);
            this.panel1.Controls.Add(this.txtChaveAES);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(493, 271);
            this.panel1.TabIndex = 22;
            // 
            // btnAESDescript
            // 
            this.btnAESDescript.FlatAppearance.BorderSize = 2;
            this.btnAESDescript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAESDescript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAESDescript.ForeColor = System.Drawing.Color.White;
            this.btnAESDescript.Location = new System.Drawing.Point(250, 218);
            this.btnAESDescript.Name = "btnAESDescript";
            this.btnAESDescript.Size = new System.Drawing.Size(216, 35);
            this.btnAESDescript.TabIndex = 4;
            this.btnAESDescript.Text = "AES - Descript";
            this.btnAESDescript.UseVisualStyleBackColor = true;
            this.btnAESDescript.Click += new System.EventHandler(this.btnAESDescript_Click);
            // 
            // btnAESCript
            // 
            this.btnAESCript.FlatAppearance.BorderSize = 2;
            this.btnAESCript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAESCript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAESCript.ForeColor = System.Drawing.Color.White;
            this.btnAESCript.Location = new System.Drawing.Point(28, 218);
            this.btnAESCript.Name = "btnAESCript";
            this.btnAESCript.Size = new System.Drawing.Size(216, 35);
            this.btnAESCript.TabIndex = 3;
            this.btnAESCript.Text = "AES - Cript";
            this.btnAESCript.UseVisualStyleBackColor = true;
            this.btnAESCript.Click += new System.EventHandler(this.btnAESCript_Click);
            // 
            // txtAESCript
            // 
            this.txtAESCript.BackColor = System.Drawing.Color.Black;
            this.txtAESCript.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAESCript.ForeColor = System.Drawing.Color.White;
            this.txtAESCript.Location = new System.Drawing.Point(250, 19);
            this.txtAESCript.Multiline = true;
            this.txtAESCript.Name = "txtAESCript";
            this.txtAESCript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAESCript.Size = new System.Drawing.Size(216, 193);
            this.txtAESCript.TabIndex = 2;
            // 
            // txtAESTexto
            // 
            this.txtAESTexto.Location = new System.Drawing.Point(28, 58);
            this.txtAESTexto.Multiline = true;
            this.txtAESTexto.Name = "txtAESTexto";
            this.txtAESTexto.Size = new System.Drawing.Size(216, 154);
            this.txtAESTexto.TabIndex = 1;
            this.txtAESTexto.Text = "Texto";
            // 
            // txtChaveAES
            // 
            this.txtChaveAES.Location = new System.Drawing.Point(28, 19);
            this.txtChaveAES.Name = "txtChaveAES";
            this.txtChaveAES.Size = new System.Drawing.Size(216, 33);
            this.txtChaveAES.TabIndex = 0;
            this.txtChaveAES.Text = "chavede16digitos";
            // 
            // frmSimetrico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 875);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmSimetrico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Oficina - Criptografia";
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnDES3Decript;
        private System.Windows.Forms.Button btnDES3Cript;
        private System.Windows.Forms.TextBox txtDES3Cript;
        private System.Windows.Forms.TextBox txtDES3Texto;
        private System.Windows.Forms.TextBox txtDES3Chave;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDESDecript;
        private System.Windows.Forms.Button btnDESCript;
        private System.Windows.Forms.TextBox txtDESCRipt;
        private System.Windows.Forms.TextBox txtDESTexto;
        private System.Windows.Forms.TextBox txtDESChave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAESDescript;
        private System.Windows.Forms.Button btnAESCript;
        private System.Windows.Forms.TextBox txtAESCript;
        private System.Windows.Forms.TextBox txtAESTexto;
        private System.Windows.Forms.TextBox txtChaveAES;
    }
}

