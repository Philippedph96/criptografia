﻿namespace Nsf._2018.Oficina.Seguranca.Criptografia
{
    partial class frmAssimetrico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGerarChaves = new System.Windows.Forms.Button();
            this.txtChaveRSAPrivada = new System.Windows.Forms.TextBox();
            this.btnRSADescript = new System.Windows.Forms.Button();
            this.btnRSACript = new System.Windows.Forms.Button();
            this.txtRSACript = new System.Windows.Forms.TextBox();
            this.txtRSATexto = new System.Windows.Forms.TextBox();
            this.txtChaveRSAPublica = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(498, 64);
            this.panel2.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Tomato;
            this.label2.Location = new System.Drawing.Point(449, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "x";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Harlow Solid Italic", 22F, System.Drawing.FontStyle.Italic);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Criptografia Assimétrica";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(134)))), ((int)(((byte)(180)))));
            this.panel1.Controls.Add(this.btnGerarChaves);
            this.panel1.Controls.Add(this.txtChaveRSAPrivada);
            this.panel1.Controls.Add(this.btnRSADescript);
            this.panel1.Controls.Add(this.btnRSACript);
            this.panel1.Controls.Add(this.txtRSACript);
            this.panel1.Controls.Add(this.txtRSATexto);
            this.panel1.Controls.Add(this.txtChaveRSAPublica);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(498, 421);
            this.panel1.TabIndex = 19;
            this.panel1.Click += new System.EventHandler(this.btnRSADescript_Click);
            // 
            // btnGerarChaves
            // 
            this.btnGerarChaves.FlatAppearance.BorderSize = 2;
            this.btnGerarChaves.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerarChaves.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerarChaves.ForeColor = System.Drawing.Color.White;
            this.btnGerarChaves.Location = new System.Drawing.Point(24, 100);
            this.btnGerarChaves.Name = "btnGerarChaves";
            this.btnGerarChaves.Size = new System.Drawing.Size(442, 35);
            this.btnGerarChaves.TabIndex = 6;
            this.btnGerarChaves.Text = "Gerar chaves";
            this.btnGerarChaves.UseVisualStyleBackColor = true;
            this.btnGerarChaves.Click += new System.EventHandler(this.btnGerarChaves_Click);
            // 
            // txtChaveRSAPrivada
            // 
            this.txtChaveRSAPrivada.Location = new System.Drawing.Point(28, 61);
            this.txtChaveRSAPrivada.Name = "txtChaveRSAPrivada";
            this.txtChaveRSAPrivada.Size = new System.Drawing.Size(438, 33);
            this.txtChaveRSAPrivada.TabIndex = 5;
            this.txtChaveRSAPrivada.Text = "Chave Privada";
            // 
            // btnRSADescript
            // 
            this.btnRSADescript.FlatAppearance.BorderSize = 2;
            this.btnRSADescript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRSADescript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRSADescript.ForeColor = System.Drawing.Color.White;
            this.btnRSADescript.Location = new System.Drawing.Point(250, 357);
            this.btnRSADescript.Name = "btnRSADescript";
            this.btnRSADescript.Size = new System.Drawing.Size(216, 35);
            this.btnRSADescript.TabIndex = 4;
            this.btnRSADescript.Text = "RSA - Descript";
            this.btnRSADescript.UseVisualStyleBackColor = true;
            this.btnRSADescript.Click += new System.EventHandler(this.btnRSADescript_Click);
            // 
            // btnRSACript
            // 
            this.btnRSACript.FlatAppearance.BorderSize = 2;
            this.btnRSACript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRSACript.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRSACript.ForeColor = System.Drawing.Color.White;
            this.btnRSACript.Location = new System.Drawing.Point(28, 357);
            this.btnRSACript.Name = "btnRSACript";
            this.btnRSACript.Size = new System.Drawing.Size(216, 35);
            this.btnRSACript.TabIndex = 3;
            this.btnRSACript.Text = "RSA - Cript";
            this.btnRSACript.UseVisualStyleBackColor = true;
            this.btnRSACript.Click += new System.EventHandler(this.btnRSACript_Click);
            // 
            // txtRSACript
            // 
            this.txtRSACript.BackColor = System.Drawing.Color.Black;
            this.txtRSACript.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRSACript.ForeColor = System.Drawing.Color.White;
            this.txtRSACript.Location = new System.Drawing.Point(250, 197);
            this.txtRSACript.Multiline = true;
            this.txtRSACript.Name = "txtRSACript";
            this.txtRSACript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRSACript.Size = new System.Drawing.Size(216, 154);
            this.txtRSACript.TabIndex = 2;
            // 
            // txtRSATexto
            // 
            this.txtRSATexto.Location = new System.Drawing.Point(28, 197);
            this.txtRSATexto.Multiline = true;
            this.txtRSATexto.Name = "txtRSATexto";
            this.txtRSATexto.Size = new System.Drawing.Size(216, 154);
            this.txtRSATexto.TabIndex = 1;
            this.txtRSATexto.Text = "Texto";
            // 
            // txtChaveRSAPublica
            // 
            this.txtChaveRSAPublica.Location = new System.Drawing.Point(28, 22);
            this.txtChaveRSAPublica.Name = "txtChaveRSAPublica";
            this.txtChaveRSAPublica.Size = new System.Drawing.Size(438, 33);
            this.txtChaveRSAPublica.TabIndex = 0;
            this.txtChaveRSAPublica.Text = "Chave Pública";
            // 
            // frmAssimetrico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 485);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmAssimetrico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Oficina - Criptografia";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGerarChaves;
        private System.Windows.Forms.TextBox txtChaveRSAPrivada;
        private System.Windows.Forms.Button btnRSADescript;
        private System.Windows.Forms.Button btnRSACript;
        private System.Windows.Forms.TextBox txtRSACript;
        private System.Windows.Forms.TextBox txtRSATexto;
        private System.Windows.Forms.TextBox txtChaveRSAPublica;
    }
}

