﻿using Nsf._2018.Oficina.Seguranca.Criptografia.Criptografia.Assimetrico;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Oficina.Seguranca.Criptografia
{
    public partial class frmAssimetrico : Form
    {
        public frmAssimetrico()
        {
            InitializeComponent();
        }

        private void btnRSACript_Click(object sender, EventArgs e)
        {
            RSACript cript = new RSACript();
            string textoCript = cript.Criptografar(txtChaveRSAPublica.Text, txtRSATexto.Text);

            txtRSACript.Text = textoCript;
        }

        private void btnRSADescript_Click(object sender, EventArgs e)
        {
            RSACript cript = new RSACript();
            string texto = cript.Descriptografar(txtChaveRSAPrivada.Text, txtRSACript.Text);

            txtRSATexto.Text = texto;
        }

        private void btnGerarChaves_Click(object sender, EventArgs e)
        {
            RSACript cript = new RSACript();
            List<string> chaves = cript.GerarChaves();

            txtChaveRSAPrivada.Text = chaves[0];
            txtChaveRSAPublica.Text = chaves[1];
        }
    }
}
